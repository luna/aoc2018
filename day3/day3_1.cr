lines = [] of String


class Claim
  def initialize(@cid : String, @p1 : UInt64, @p2 : UInt64,
                 @width : UInt64, @height : UInt64)
  end
end

cloth = [] of Array(Array(Claim))
claims = [] of Claim

# initialize cloth
(0...1000).each do |i|
  cloth_line = [] of Array(Claim)

  (0...1000).each do |j|
    # cloth_line << Claim.new("-1", 0, 0, 0, 0)
    cloth_line << [] of Claim
  end
  
  cloth << cloth_line
end

while true
  line = gets '\n'

  if line.nil?
    break
  end

  lines << line.strip().gsub(" ", "")
end


lines.each do |line|
  cid_p, claim_data = line.split('@')
  p1p2, wh = claim_data.split(':')

  p1, p2 = p1p2.split(',')
  width, height = wh.split('x')
  cid = cid_p.gsub("#", "")

  claims << Claim.new(cid, p1.to_u64, p2.to_u64, width.to_u64, height.to_u64)
end

claims.each do |claim|
  (claim.@p1...claim.@p1 + claim.@width).each do |x|
    (claim.@p2...claim.@p2 + claim.@height).each do |y|
      cloth[x][y] << claim
    end
  end
end

# check the cloth
overlaps = 0

(0...1000).each do |i|
  (0...1000).each do |j|
    claims = cloth[i][j]
    if claims.size > 1
      overlaps += 1
    end
  end
end

puts overlaps
