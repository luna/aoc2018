import pprint
from collections import namedtuple

Claim = namedtuple('Claim', 'cid p1 p2 width height')

def main():
    claims = []

    cloth = []

    print('allocating..')

    for i in range(1000):
        cloth.insert(i, [])
        for j in range(1000):
            cloth[i].insert(j, None)

    print('allocation done')

    while True:
        try:
            claim_line = input('>').replace(' ', '')
        except EOFError:
            break

        cid_p, params = claim_line.split('@')
        p1p2, wh = params.split(':')
        p1, p2 = p1p2.split(',')
        w, h = wh.split('x')

        cid = cid_p.replace('#' , '')
        c = Claim(cid, int(p1), int(p2), int(w), int(h))
        claims.append(c)

    print('running claims')

    for claim in claims:
        for i in range(claim.p1, claim.p1 + claim.width):
            for j in range(claim.p2, claim.p2 + claim.height):
                if cloth[i][j] is None:
                    cloth[i][j] = []

                cloth[i][j].append(claim)

    overlap = 0

    for i in range(1000):
        for j in range(1000):
            if len(cloth[i][j] or []) > 1:
                overlap += 1

    print('overlap', overlap)

main()
