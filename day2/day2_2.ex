input = IO.gets("input?")
        |> String.trim
        |> String.split(" ")

defmodule Solver do

  def to_list(string) do
    case string do
      "" -> []
      _ ->
        string
        |> String.split("")
        |> Enum.filter(fn char ->
          char != ""
        end)
    end
  end

  # get the common string between the box ids
  def get_common(box_id, box_id_2) do

    # convert to list
    box_id_l = to_list(box_id)
    box_id_2_l = to_list(box_id_2)

    # zip them
    list_chars = Enum.zip(box_id_l, box_id_2_l)

    # if theyre equal, we add to the accumulator
    common_str = Enum.reduce(list_chars, "", fn tup, acc ->
      {c1, c2} = tup

      if c1 == c2 do
        acc <> c1
      else
        acc
      end
    end)

    # for common_str to be valid it has to be
    # L(common_str) = L(box_id) - 1
    # since correct box ids have just *ONE*
    # character difference between them
    if String.length(common_str) == (String.length(box_id) - 1) do
      {:ok, common_str}
    else
      :not_found
    end

  end

  def solve(input) do
    # find all the common strings
    # and filter shit
    commons = Enum.map(input, fn box_id ->
      Enum.map(input, fn box_id_2 ->
        common = get_common(box_id, box_id_2)

        case common do
          :not_found -> nil
          {:ok, str} -> str
        end
      end)
      |> Enum.filter(fn combo ->
        combo != nil
      end)
    end)
    |> Enum.filter(fn combo ->
      combo != []
    end)

  end

end

res = Solver.solve(input)
IO.puts "result: #{inspect res}"
