input = IO.gets("input?")
        |> String.trim
        |> String.split(" ")

defmodule Solver do

  # convert from bool to 1/0
  def to_num(boolean) do
    if boolean do
      1
    else
      0
    end
  end

  def process(input) do
    ids_properties = Enum.map(input, fn box_id ->

      # convert string to list
      box_id_chars = box_id |> String.split("") |> Enum.filter(fn char ->
        char != ""
      end)

      # get the letter repeats for each char in it
      counts = Enum.map(box_id_chars, fn id_letter ->

        count = Enum.count(box_id_chars, fn character ->
          character == id_letter
        end)

        count
      end)

      IO.puts "counts: #{inspect counts}"

      # check if it has letters repeated twice or three times
      r = {
        Enum.count(counts, fn count -> count == 2 end) > 1,
        Enum.count(counts, fn count -> count == 3 end) > 1
      }

      IO.puts "result: #{inspect r}"

      r
    end)

    # count all box ids that have twice/three-repeated
    {total_twice, total_three} = Enum.reduce(ids_properties, {0, 0}, fn property, acc ->
      {old_twice, old_three} = acc
      {is_twice, is_three} = property

      change_tw = to_num(is_twice)
      change_th = to_num(is_three)

      {
        old_twice + change_tw,
        old_three + change_th
      }
    end)

    # Solution
    total_twice * total_three
  end

end

IO.puts "input: #{input}"
result = Solver.process(input)
IO.puts "ACTUAL RESULT: #{result}"
