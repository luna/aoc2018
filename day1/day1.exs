# input treatment
input = IO.gets("input?")
        |> String.trim
        |> String.split(" ")
        |> Enum.map(fn part ->
          String.to_integer part
        end)

res = Enum.sum(input)
IO.puts "answer: #{res}"
